# GISAID2ENA submission

ENA data submission from TSV metadata.
Historically, based on [a project](http://gitlab.isb-sib.ch/SPSP/sarscov2/gisaid2ena-submission) to work specifically with Gisaid data, but the code is now generic enough to
work with any data in TSV format.

## Overview

Given:
- a configuration file (YAML) ([example](test/FHI/submission.yaml))
- an input file containing one to many records metadata (TSV) ([example](test/FHI/metadata.tsv))
- a directory containing FASTQ files ([example](test/FHI/fastq))  
  Note: FASTA submission is also possible and has been half-implemented (commented out in the code).

, publish to ENA all the records (creating project, sample and analysis automatically if they do not exist yet) and returns a list of created accessions looking like this:


| sample_alias                                  | timestamp           | project_acc | sample_acc  | insdc_sample_acc | run_genome_acc | exp_genome_acc |
|-----------------------------------------------|---------------------|------------|-------------|----------------|--------------|--------------|
| SARS-CoV-2/human/Norway/6306/2020/1#908448061 | 2022-05-05T11:39:37 | PRJEB49305 | ERS11824532 | SAMEA14221344  |              |              |
| SARS-CoV-2/human/Norway/6329/2020/1#364784021 | 2022-05-05T11:39:37 | PRJEB49305 | ERS11824533 | SAMEA14221345  |              |              |

A JSON file is also available.

## Quick start

Let us submit some samples to the test submission service:
- Make sure Docker is installed on your system
- Clone or download this repository
- `cd` to `test/FHI`
- run: `docker run --rm -v $(pwd):/data:rw -e ENA_USER -e ENA_PWD --user $(id -u) registry.gitlab.com/uit-sfb/gisaid2ena:master /data/submission.yaml`

After execution, you should find `out.json` and `out.tsv` receipt files in the `test/FHI` directory.

## Usage

### Configuration file

The configuration file gives the script information about the context so that it can interpret properly the input file and produces the desired output.
An example is provided [here](test/FHI/submission.yaml) including detailed explicative comments that should be sufficient to be able to write your own configuration.

Note: By itself, the submission script does not download or move any file. This needs to be implemented using the 4 hooks:
- init_script
- before_script
- after_script
- cleanup_script

### Using Docker (highly recommended)

Export envirs `ENA_USER` and `ENA_PWD` (ENA username and ENA password respectively which you use to log in [here](https://www.ebi.ac.uk/ena/submit/webin/login)).

Assuming all the files are located in a directory as in the [test directory](test/FHI),
`cd` to this directory, then:
`docker run --rm -v $(pwd):/data:rw -e ENA_USER -e ENA_PWD --user $(id -u) registry.gitlab.com/uit-sfb/gisaid2ena:master [options] /data/<submission.yaml>`

Note: You can replace `master` with a [revision tag](https://gitlab.com/uit-sfb/gisaid2ena/-/releases).

Run with `-h` for the list of available options.

### Using the binary (not recommended)

#### Requirements

- Java 1.8+
- Python 3.7+
- ENA webin-cli:  
  `URL=$(curl --silent "https://api.github.com/repos/enasequence/webin-cli/releases/latest" | jq -r '.assets[0].browser_download_url'); curl -L "$URL" > webin-cli.jar`
- Some python libs:
```
sudo apt install python3-pip
pip3 install wheel biopython xlrd requests
```

#### Usage

Export envirs `ENA_USER`, `ENA_PWD` and `WEBIN_CLI_PATH`.

`./gisaid2ena.py [options] <pathToSubmissionFile>`

Run `./gisaid2ena.py -h` for the list of available options.

An example of submission file can be found [here](test/FHI/submission.yaml).

### Test setup

By using `--test` flag or setting `force_test` to true in the submission file,
the records are published on the test EBI server: https://wwwdev.ebi.ac.uk/ena/submit/webin/report/samples;defaultSearch=true.
In addition, a timestamp is added to the `sample_alias` in order to make it unique (otherwise publication fails). 

The regular server URL is: https://www.ebi.ac.uk/ena/submit/webin/report/samples;defaultSearch=true.

## Resources

- [EBI API doc](https://ena-docs.readthedocs.io/en/latest/submit/general-guide.html)
