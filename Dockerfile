FROM korekontrol/ubuntu-java-python3:latest
#Requires python 3.7+

RUN apt update && apt install curl jq openssh-client -y
RUN pip3 install biopython xlrd pyyaml requests

#RUN URL=$(curl --silent "https://api.github.com/repos/enasequence/webin-cli/releases/latest" | jq -r '.assets[0].browser_download_url'); curl -L "$URL" > webin-cli.jar
RUN curl -L https://github.com/enasequence/webin-cli/releases/download/v4.2.3/webin-cli-4.2.3.jar > webin-cli.jar; java -jar webin-cli.jar -help > /dev/null #We check that we do have a working executable

ENV WEBIN_CLI_PATH /webin-cli.jar

COPY gisaid2ena.py .
RUN chmod +x gisaid2ena.py


ENTRYPOINT ["python3", "-u", "gisaid2ena.py", "-o", "/data"]
#We usitilise "-u" to prevent Python from buffering stdout and stderr
CMD ["/data/submission.yaml"]