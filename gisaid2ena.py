#!/usr/bin/env python3

import math
import random
from pathlib import Path
from datetime import datetime, timedelta
import os
import re
import requests
import xml.etree.ElementTree as xmlTree
import yaml
import json
import csv
#from Bio import SeqIO
#import gzip
import shutil
import subprocess
from shutil import rmtree
import argparse
import multiprocessing as mp
from functools import partial
import sys
import traceback
import smtplib
from email.message import EmailMessage

success = True


class Context:
    is_dev = True

    def __init__(self, workdir, output, test, delay, parallelism, submission_file, config):
        self.ena_user = os.environ["ENA_USER"]
        self.ena_pwd = os.environ["ENA_PWD"]
        self.webin_cli_path = os.environ.get("WEBIN_CLI_PATH", "/webin-cli.jar")
        self.delay = delay
        self.parallelism = parallelism
        self.workdir = Path(workdir).resolve()
        self.output = Path(output).resolve()
        self.submission_file = submission_file
        self.study_settings = config["study"]
        self.email_settings = config["email"]

        settings = config["settings"]
        self.metadata_file = Path(settings["metadata"]).resolve()
        #if settings["fasta"]:
        #    self.fasta_file = Path(settings["fasta"]).resolve()
        #else:
        #    self.fasta_file = ""
        if settings["fastq"]:
            self.fastq_dir = Path(settings["fastq"]).resolve()
        else:
            self.fastq_dir = ""
        self.init_script = settings["init_script"] if "init_script" in settings else ""
        self.before_script = settings["before_script"] if "before_script" in settings else ""
        self.after_script = settings["after_script"] if "after_script" in settings else ""
        self.cleanup_script = settings["cleanup_script"] if "cleanup_script" in settings else ""
        self.input_folder = os.path.join(workdir, 'INPUT')
        self.output_folder = os.path.join(workdir, 'OUTPUT')
        self.study_accession = self.study_settings["accession"] if "accession" in self.study_settings else ""
        if settings['force_test']:
            print("Forcing --test")
            Context.is_dev = True
        else:
            Context.is_dev = test
        if Context.is_dev:
            self.url = 'https://wwwdev.ebi.ac.uk/ena/submit/drop-box/submit/'
        else:
            self.url = 'https://www.ebi.ac.uk/ena/submit/drop-box/submit/'

        clear_folder_content(self.input_folder)
        clear_folder_content(self.output_folder)

        # We make sure the output file is deleted before executing anything
        if os.path.exists(self.output / "out.json"):
            os.remove(self.output / "out.json")

        if os.path.exists(self.output / "out.tsv"):
            os.remove(self.output / "out.tsv")

        self.output.mkdir(parents=True, exist_ok=True)

        print("parallelism: " + str(parallelism))


        self.mappings = config["mappings"]

        print("Loading metadata file...")
        # Panda cannot deal with spaces in header
        #df = pandas.read_csv(self.metadata_file, sep='\t', header=0)

        def format_unknown(v):
            return "not provided" if v in ["Unknown", "unknown", "", "NaN"] else v

        ds = []
        with open(self.metadata_file, 'r') as f:
            r = csv.reader(f, delimiter='\t')
            header = next(r)
            for row in r:
                values = list(map(lambda s: format_unknown(s.strip()), row))
                dic = dict(zip(header, values))
                d = {}
                for k, v in self.mappings.items():
                    d[k] = convert_mapping(v, dic)
                if Context.is_dev:  # add a random int to the sample alias, otherwise if it already exists it will be refused
                    d['sample_alias'] = d['sample_alias'] + '#' + str(random.randrange(1000000000))
                ds.append(d)

        self.metadata = ds

    def clean(self):
        shutil.rmtree(self.input_folder)
        shutil.rmtree(self.output_folder)

    def update_project_accession(self, project_accession):
        self.study_accession = project_accession


def convert_mapping(v, dic, emptyIfNotFound=True):
    def replace(match):
        key = match.group(1)
        if key in dic:
            return dic[key]
        else:
            return "" if emptyIfNotFound else f"${{{key}}}"
    return re.sub(r'\${([^{}]*)}', replace, str(v))


def is_not_blank(s):
    return bool(s and s.strip())


def submit_metadata(files, id, context):
    response = requests.post(context.url, files=files, auth=(context.ena_user, context.ena_pwd))
    if not response.ok:
        raise Exception('\nOops:\n' + response.text)
    receipt = response.content
    #print(receipt)

    # write log in OUTPUT dir
    log_filename = os.path.join(context.output_folder, f'ENA_receipt_{id}.log')
    parentDir = os.path.dirname(log_filename)
    if not os.path.exists(parentDir):
        os.makedirs(parentDir)
    with open(log_filename, 'a+') as file:
        file.write(receipt.decode('utf-8'))
        file.write('\n{0}\n'.format('*' * 80))
    return receipt


def create_add_submission_xml(schema, hold_date):
    return f"""<?xml version='1.0' encoding='UTF-8'?>
        <SUBMISSION>
          <ACTIONS>
            <ACTION>
              <ADD schema="{schema}"/>
            </ACTION>
            <ACTION> <HOLD HoldUntilDate = "{hold_date}"/> </ACTION >
          </ACTIONS>
        </SUBMISSION>"""


def submit_data(submission_type, data_xml, id, context):
    date_str = (datetime.today() + timedelta(days=context.delay)).strftime('%Y-%m-%d')
    submission_xml = create_add_submission_xml(submission_type.lower(), date_str)
    manifest_filename = os.path.join(context.input_folder, submission_type + "_" + id) + '_submission.xml'
    parentDir = os.path.dirname(manifest_filename)
    if not os.path.exists(parentDir):
        os.makedirs(parentDir)
    with open(manifest_filename, "w") as file:
        file.write(submission_xml)
    manifest_filename = os.path.join(context.input_folder, submission_type + "_" + id) + '_submission_data.xml'
    with open(manifest_filename, "w") as file:
        file.write(data_xml)
    #print(submission_xml)
    files = {
        'SUBMISSION': ('.xml', submission_xml),
        submission_type: ('.xml', data_xml)
    }

    receipt = submit_metadata(files, id, context)
    return process_receipt(receipt, submission_type)


def submit_release(data_accession, alias, center_name, context):
    release_xml = create_release_xml(alias, center_name, data_accession)
    files = {
        'SUBMISSION': ('.xml', release_xml),
    }
    receipt = submit_metadata(files, alias, context)
    receipt_root = xmlTree.fromstring(receipt)
    res = receipt_root.get('success')
    if res.lower() == 'true':
        return True

    error = receipt_root.find('MESSAGES/ERROR')
    if error.text.lower().find('only private') >= 0:
        return True  # "Failed to release ... Only private or cancelled objects may be released"
    raise Exception('\nOops:\n' + error.text)


def process_receipt(receipt, submission_type):
    receipt_root = xmlTree.fromstring(receipt)
    res = receipt_root.get('success')
    if res.lower() == 'true':
        elem = receipt_root.find(submission_type)
        return elem.get('accession'), elem.find('EXT_ID').get('accession')
    else:
        first_error = receipt_root.find('MESSAGES/ERROR')
        match_alias = re.search(r'In sample, alias:"(\S+)"', first_error.text)
        match_accession = re.search(r'The object being added already exists in the submission account with accession: "(\w+)"\.', first_error.text)
        if match_accession:
            if match_alias:
                alias = match_alias.group(1)
            else:
                alias = ""
            accession = match_accession.group(1)
            print(f"{submission_type} {alias} already exists ({accession}). Skipping creation.")
            return accession, ""
    errors = receipt_root.findAll('MESSAGES/ERROR').map(lambda err: err.text)
    raise Exception('\n'.join(map(str, errors)))


def create_project_description_xml(alias, center_name, title, description):
    return f"""<?xml version='1.0' encoding='UTF-8'?>
        <PROJECT_SET>
            <PROJECT alias="{alias}" center_name="{center_name}">
                <TITLE>{title}</TITLE>
                <DESCRIPTION>{description}</DESCRIPTION>
                <SUBMISSION_PROJECT>
                    <SEQUENCING_PROJECT/>
                </SUBMISSION_PROJECT>
            </PROJECT>
        </PROJECT_SET>
        """


def create_release_xml(alias, center_name, accession):
    return f"""<?xml version='1.0' encoding='UTF-8'?>
        <SUBMISSION alias="{alias}" center_name="{center_name}">
            <ACTIONS>
                <ACTION>
                    <RELEASE target="{accession}"/>
                </ACTION>
            </ACTIONS>
        </SUBMISSION>
        """


def create_sample_description_xml(record, center_name):
    def add(parent, tag, value):
        xmlTree.SubElement(parent, tag).text = value

    def add_attribute(tag, mandatory=False):
        def add_attr(tag, value):
            attribute = xmlTree.SubElement(sample_attributes, 'SAMPLE_ATTRIBUTE')
            add(attribute, 'TAG', tag)
            add(attribute, 'VALUE', value)
        if record[tag] and is_not_blank(record[tag]):
            add_attr(tag, record[tag])
        elif mandatory:
            add_attr(tag, "not provided")

    sample_alias = record['sample_alias']
    sample = xmlTree.Element('SAMPLE', alias=sample_alias, center_name=center_name)

    add(sample, 'TITLE', sample_alias)
    sample_name = xmlTree.SubElement(sample, 'SAMPLE_NAME')
    add(sample_name, 'TAXON_ID', str(record['TAXON_ID']))
    sample_attributes = xmlTree.SubElement(sample, 'SAMPLE_ATTRIBUTES')
    add_attribute('ENA-CHECKLIST')
    add_attribute('collection date')
    add_attribute('collecting institution')
    add_attribute('collector name', True)
    add_attribute('geographic location (country and/or sea)')
    add_attribute('geographic location (region and locality)')
    add_attribute('geographic location (latitude)')
    add_attribute('geographic location (longitude)')
    add_attribute('sample capture status')
    add_attribute('host disease outcome')
    add_attribute('virus identifier')
    add_attribute('receipt date')
    add_attribute('definition for seropositive sample')
    add_attribute('serotype (required for a seropositive sample)')
    add_attribute('host habitat')
    add_attribute('isolation source host-associated')
    add_attribute('host behaviour')
    add_attribute('isolation source non-host-associated')
    add_attribute('host common name')
    add_attribute('host scientific name')
    add_attribute('host subject id', True)
    add_attribute('host health state', True)
    add_attribute('host sex', True)
    add_attribute('host age')
    add_attribute('isolate')
    add_attribute('GISAID Accession ID')
    add_attribute('Authors')
    xml = xmlTree.tostring(sample, encoding='unicode')

    return xml


#def split_multi_fasta(context):
#    """
#    Extract multiple sequence fasta file and write each sequence in separate file.
#    Modified from: https://www.biostars.org/p/340937/
#
#    ! IMPORTANT !
#    - We assume that we have complete genomes in multi-FASTA.
#    - Since these are assumed to be complete genomes, we submit as chromosome => need to make it more general
#    - Need to standardize also virus names (cf. seq_rec.id below)
#    """
#
#    def gzip_file(filepath):
#        with open(filepath, 'rb') as f_in:
#            with gzip.open(filepath + '.gz', 'wb') as f_out:
#                shutil.copyfileobj(f_in, f_out)
#
#    if context.fasta_file:
#        with open(context.fasta_file) as f_in:
#            record = SeqIO.parse(f_in, "fasta")
#            #file_count = 0
#            for seq_rec in record:
#                #file_count += 1
#                virus_name = seq_rec.id.replace('/', '_')
#                virus_path = os.path.join(context.input_folder, virus_name)
#                fasta_fn = virus_path + '.fasta'
#                chromosome_fn = virus_path + '_chr.txt'  # TODO: currently assumes there's only 1 segment, i.e. complete genome
#                seq_rec.description = ''
#                #seq_rec.id = virus_name + '-' + str(file_count) #???
#                with open(fasta_fn, 'w') as f_out:
#                    SeqIO.write(seq_rec, f_out, 'fasta')
#                gzip_file(fasta_fn)
#
#                with open(chromosome_fn, 'w') as f_out:
#                    f_out.write(f'{seq_rec.id}\t1\tLinear-Chromosome')
#                gzip_file(chromosome_fn)
#
#        if file_count == 0:
#            raise Exception(f'No valid sequence in fasta file "{multi_fasta_name}"')
#        return 'Done splitting multi-FASTA file and writing associated chromosome files.'
#
#
#def write_assembly_manifest_file(study_accession, sample_acc, sample_alias, record, context):
#    virus_name = record['virus name'].replace('/', '_')
#    data = [
#        ('STUDY', study_accession),
#        ('SAMPLE', sample_acc),
#        ('ASSEMBLYNAME', sample_alias),
#        ('ASSEMBLY_TYPE', 'COVID-19 outbreak'),
#        ('COVERAGE', record['coverage']),
#        ('PROGRAM', record['library_construction protocol']),
#        ('PLATFORM', record['sequencing method']),
#        ('MOLECULETYPE', 'genomic RNA'),
#        ('FASTA', virus_name + '.fasta.gz'),
#        ('CHROMOSOME_LIST', virus_name + '_chr.txt.gz')]
#
#    text = ''
#    for tag, value in data:
#        text += f'{tag}\t{value}\n'
#    manifest_filename = os.path.join(context.input_folder, virus_name) + '_assembly_MANIFEST.txt'
#    with open(manifest_filename, "w") as file:
#        file.write(text)
#
#    return manifest_filename

def write_raw_reads_manifest_file(study_accession, sample_acc, record, context):
    data = [
        ('STUDY', study_accession),
        ('SAMPLE', sample_acc),
        ('NAME', record['sample_alias']),
        ('INSTRUMENT', record['INSTRUMENT']),
        ('INSERT_SIZE', record['INSERT_SIZE']),
        ('LIBRARY_SOURCE', record['LIBRARY_SOURCE']),
        ('LIBRARY_SELECTION', record['LIBRARY_SELECTION']),
        ('LIBRARY_STRATEGY', record['LIBRARY_STRATEGY']),
        ('FASTQ', context.fastq_dir / record['fastq_name_R1']),
        ('FASTQ', context.fastq_dir / record['fastq_name_R2'])]

    text = ''
    for tag, value in data:
        text += f'{tag}\t{value}\n'
    manifest_filename = os.path.join(context.input_folder, record['sample_alias']) + '_raw_reads_MANIFEST.txt'
    parentDir = os.path.dirname(manifest_filename)
    if not os.path.exists(parentDir):
        os.makedirs(parentDir)
    with open(manifest_filename, "w") as file:
        file.write(text)

    return manifest_filename


def run_cmd(cmds, throw=True):
    """Execute command line. In case of error, stderr and stdout are printed.
    :param cmds: the string of command line
    :param throw: When True, error message is printed and an exception is thrown
    :return output: the string of output from execution (stderr + stdout)
    From: https://github.com/usegalaxy-eu/ena-upload-cli/blob/master/ena_upload/ena_upload.py
    """
    output = ""
    ret = subprocess.run(cmds, capture_output=True, shell=True)
    if (len(ret.stderr) > 0):
        stderr_str = ret.stderr.decode("utf-8")
        output += stderr_str
    if (len(ret.stdout) > 0):
        std_out = ret.stdout.decode("utf-8")
        output += std_out
    if ret.returncode != 0:
        if throw:
            print(output)
            ret.check_returncode()
    return output


#Upload sequences
def submit_data_webin_cli(type, manifest_filename, center_name, context):
    try:
        cmd = f'java -jar {context.webin_cli_path} -context {type} -userName "{context.ena_user}" -password "{context.ena_pwd}"' \
              f' -centerName "{center_name}" -manifest "{manifest_filename}" -inputDir "{context.input_folder}" -outputDir "{context.output_folder}/" -submit'
        if Context.is_dev:
            cmd += ' -test'
        receipt = run_cmd(cmd, False)
        message = receipt.lower()
        already_exists = message.find('already exists') >= 0
        if message.find('completed successfully') >= 0 or already_exists:
            run_accession_arr = re.compile('ERR[0-9]{6,}').findall(receipt)
            if run_accession_arr:
                run_accession = run_accession_arr[0]
            else:
                run_accession = ""
            experiment_accession_arr = re.compile('ERX[0-9]{6,}').findall(receipt)
            if experiment_accession_arr:
                experiment_accession = experiment_accession_arr[0]
            else:
                experiment_accession = ""
            match_alias = re.search(r'In experiment, alias:"(\S+)"', receipt)
            if match_alias:
                alias = match_alias.group(1)
            else:
                alias = ""
            if already_exists:
                print(f"Experiment {experiment_accession} already exists for sample {alias}. Skipping creation.")
            return run_accession, experiment_accession
        # Something went wrong but we do want to continue the submission. The run and experiment accessions will simply be empty.
        print(receipt)
        return "", ""
    except Exception:
        traceback.print_exc()


def clear_folder_content(folder):
    if os.path.isdir(folder):
        rmtree(folder)
    os.makedirs(folder)


def init_context(args):
    with open(args.submission_file, 'r') as file:
        config = yaml.safe_load(file)

    return Context(args.workdir,
                   args.output,
                   args.test,
                   args.delay,
                   args.parallel,
                   args.submission_file,
                   config)


# If study not defined, we create one and update submission_file
def create_study(context):
    study_accession = context.study_accession
    if not is_not_blank(study_accession):
        print("Creating study...")
        center_name = context.study_settings['center_name']
        study_alias = context.study_settings['alias']
        description_xml = create_project_description_xml(study_alias, center_name,
                                                         context.study_settings['title'], context.study_settings['description'])
        study_accession = submit_data('PROJECT', description_xml, study_alias, context)[0]
        submit_release(study_accession, study_alias, center_name, context)

        context.study_settings['accession'] = study_accession
        with open(context.submission_file, 'a') as file:
            file.write("\n  accession: " + study_accession) #Two spaces needed for including it under the project object in Yaml
    else:
        print(f"Study {study_accession} already exists. Skipping creation.")

    context.update_project_accession(study_accession)


# Register one sample and submit raw reads
def submit_sample(record, context):
    global success
    try:
        res = []
        center_name = context.study_settings['center_name']
        study_accession = context.study_accession
        sample_alias = record['sample_alias']
        before_script = convert_mapping(context.before_script, record, False)
        #print(f"Downloading sequences ({sample_alias})...")
        print(run_cmd(before_script))
        print("Submitting sample " + sample_alias + " ...")
        sample_xml = create_sample_description_xml(record, center_name)  # return description xml
        sample_accession, insdc_sample_id = submit_data('SAMPLE', sample_xml, sample_alias, context)
        submit_release(sample_accession, sample_alias, center_name, context)

        sample_dict = {
            'sample_alias': sample_alias, #Must be in first position
            'timestamp': datetime.today().strftime("%Y-%m-%dT%H:%M:%S"),
            'project_acc': context.study_accession,
            'sample_acc': sample_accession,
            'insdc_sample_acc': insdc_sample_id
        }

        #            if context.fasta_file:
        #                print("Submitting FASTA...")
        #                manifest_filename = write_assembly_manifest_file(study_accession, sample_accession, sample_alias, record, context)
        #                run_acc, exp_acc = submit_data_webin_cli("genome", manifest_filename, center_name, context)
        #                res.append({
        #                    **sample_dict, **{
        #                    'run_genome_acc': run_acc,
        #                    'exp_genome_acc': exp_acc,
        #                }
        #                            })

        if context.fastq_dir:
            #print("Submitting FASTQ...")
            manifest_raw_reads_filename = write_raw_reads_manifest_file(study_accession, sample_accession, record, context)
            run_acc, exp_acc = submit_data_webin_cli("reads", manifest_raw_reads_filename, center_name, context)
            res.append({
                **sample_dict, **{
                    'run_genome_acc': run_acc,
                    'exp_genome_acc': exp_acc,
                }
            })

        after_script = convert_mapping(context.after_script, record, False)
        with lock:
            if res:
                with open("tmp.tsv", 'w') as output_file:
                    dw = csv.DictWriter(output_file, res[0].keys(), delimiter='\t')
                    dw.writeheader()
                    dw.writerows(res)
            print(run_cmd(after_script))

    except Exception:
        # This would not work, as processes receive an immutable copy
        # success = False
        traceback.print_exc()

    # return a list of dicts (we use List as an Option)
    return res


def submit_parallel(df, context, results, pool):
    submit_partial = partial(submit_sample, context=context)
    chunk_size = math.ceil(max(len(df) / pool._processes, 1))
    print(f"Chunk size: {chunk_size}")
    # process in parallel
    results_to_add = pool.imap_unordered(submit_partial, df, chunk_size)
    # flatten result list (since we use List as an Option)
    flatten = lambda lst: [item for sublist in lst for item in sublist]
    # add dict to the list of results, modify variable
    results.extend(flatten(results_to_add))


def send_email(email_context, nb):
    if email_context["smtp_server"] and email_context["recipients"]:
        msg = EmailMessage()
        msg.set_content(f'{nb} {email_context["submission_name"]} samples have been submitted to ENA.')

        msg['Subject'] = f'{email_context["submission_name"]} ENA submission'
        msg['From'] = email_context["sender"]
        msg['To'] = ", ".join(email_context["recipients"])
        s = smtplib.SMTP(email_context["smtp_server"])
        s.send_message(msg)
        s.quit()


def main():
    global success
    parser = argparse.ArgumentParser(description="Convert GISAID format to ENA and publish study/samples to ENA.")
    parser.add_argument("--test", help="mock publishing (uses ENA test API)", action="store_true")
    parser.add_argument("--clean", help="clean workdir after execution", action="store_true")
    parser.add_argument("--delay", help="delay expressed in days before data are made public", type=int, default=0)
    parser.add_argument("--workdir", help="path to working directory", default="/tmp")
    parser.add_argument("-o", "--output", help="path to output directory", default=".")
    parser.add_argument("-p", "--parallel", help="parallelism (default: #CPU - 1)", type=int, default=mp.cpu_count()-1)
    parser.add_argument("submission_file", help="path to submission file")
    args = parser.parse_args()

    context = init_context(args)
    print(run_cmd(context.init_script))

    #print(f"Starting batch submission...")
    #run_cmd(context.cstarting_script)

    results = []
    create_study(context)

    #split_multi_fasta(context)
    #metadata = load_metadata_gisaid(context)

    num_proc = context.parallelism

    def initializer(mutex):
        global success, lock
        # No need to pass the mutex on to the partial function: lock will be made available to the global scope of each worker
        lock = mutex
    mutex = mp.Lock()

    pool = mp.Pool(num_proc, initializer=initializer, initargs=(mutex,))
    submit_parallel(context.metadata, context, results, pool)

    results_json = json.dumps(results, indent=2)
    with open(context.output / "out.json", 'w') as file:
        file.write(results_json)

    if results:
        with open(context.output / "out.tsv", 'w') as output_file:
            dw = csv.DictWriter(output_file, results[0].keys(), delimiter='\t')
            dw.writeheader()
            dw.writerows(results)
        send_email(context.email_settings, len(results))

    if success:
        print('\nDONE')
    else:
        print('\nSome submission failed. See standard output.')

    print('Summary written to ' + str(context.output))

    if args.clean:
        context.clean()
    else:
        print('Please refer to the OUTPUT/ folder in workdir for ENA receipts and other logs.')

    print(run_cmd(context.cleanup_script))

    if not success:
        sys.exit(1)


if __name__ == "__main__":
    main()

